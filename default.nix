with import <aucampia-nixpkgs> {};
rec {
  doubango-dev = doubango-generic.override { doubangoConfig = { src = ../doubango/doubango; suffix="-ia-dev"; }; };
  webrtc2sip-dev = webrtc2sip-generic.override { doubango = doubango-dev; webrtc2sipConfig = { src = ../webrtc2sip; suffix="-ia-dev"; }; };
}
